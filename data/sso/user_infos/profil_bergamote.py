# -*- coding: utf-8 -*-
from scribe.linker import user_factory

def calc_info(user_infos):
    """calcule le partage"""
    # vérification des groupes de l'utilisateur

    shares = []
    uid = user_infos.get('uid', [''])[0]
    ldapuser = user_factory(uid)
    ldapuser.ldap_admin.connect()
    user_groups = ldapuser._get_user_groups(uid)
    for group in user_groups:
        for share, sharedir in ldapuser._get_group_sharedirs(group):
            if share not in ['icones$', 'groupes']:
                shares.append(share)
    return shares

