#!/bin/sh

for dir in `ls`; do
    if [ -d "$dir" ]; then
        ldir="../src/locale/$dir/LC_MESSAGES"
	mkdir -p "$ldir"
        msgfmt "$dir/bergamote.po" -o "$ldir/bergamote.mo"
    fi
done
