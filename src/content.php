<?php
/* Begamote, the file search engine for EOLE
 * Copyright (C) 2013 Emmanuel Garette
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function error($message) {
    /*Method call when are error occured.
    Error are display in 'content' div and exit.
    */
    print "<div id='content'><b>$message</b><br /><a href='/bergamote/'>" . _("back") . "</a></div>";
    exit(1);
}

class FieldSet {
    //fieldset content
    var $content;

    function FieldSet($config) {
        /*Build FieldSet and display it. FieldSet is the options/query area.
        */
        $this->config = $config;
        $this->content = "<fieldset id='fieldset'><form action=\"/bergamote/\">";
        $this->add_input('text', 'search', $config->query_string_orig, 'search',
                    _("Find&nbsp;local&nbsp;file"));
        $this->add_input('submit', null, _("Search"), "submit");
        $this->content .= "<div id='options'><div id='config'><h1 id='options_h1'>" . _('options') . "</h1><div id='options_span'>";
        $this->content .= '<span>' . _("Results number");
        $this->content .= "\n<select name=\"step\">";
        for ($i = 10; $i <= 100; $i+=10) {
            $this->add_option($i, $this->config->query_step);
        }
        $this->content .= "</select>\n";
        $this->content .= _('Sort by ');
        $this->content .= '<select name="sort">';
        $this->add_option(_('relevant'), $this->config->query_sort);
        $this->add_option(_('date'), $this->config->query_sort);
        $this->add_option(_('size'), $this->config->query_sort);
        $this->content .= "</select></span></div></div><div id='what'><h1 id='what_h1'>" . _('contents') . "</h1><div id='what_span'>\n";
        if ($config->config['index_filename'] == 1) {
            $this->add_input("checkbox", "options[]", "filename", null, null,
                    in_array('filename', $config->options), _("Files&nbsp;name"));
            $this->add_input("checkbox", "options[]", "file", null, null,
                    in_array('file', $config->options), _("Files&nbsp;content"));
        }
        $this->content .= '</div></div><div id="where"><h1 id="where_h1">' . _('directories') . '</h1><div id="where_span">';
        if (count($config->available_dirs) != 1)
            for ($i = 0; $i < count($config->available_dirs); ++$i) {
                $group = $config->available_dirs[$i];
                $this->add_input("checkbox", "dirs[]", $group, null, null,
                        in_array($group, $config->dirs), "{$group}");
            }

        $this->content .= "</div></div></div>\n</form>\n</fieldset>\n";
    }
    function add_input($type, $name=null, $value=null, $id=null, $placeholder=null, $checked=False, $content=null) {
        /*Build input tag.
        */
        $this->content .= "<span><input type=\"{$type}\"";
        if ($name)
            $this->content .= " name=\"{$name}\"";
        if ($value)
            $this->content .= " value=\"{$value}\"";
        if ($id)
            $this->content .= " id=\"{$id}\"";
        if ($placeholder)
            $this->content .= " placeholder=\"{$placeholder}\"";
        if ($checked)
            $this->content .= ' checked="checked"';
        $this->content .= "/>";
        if ($content)
            $this->content .= $content;
        $this->content .= "</span>\n";
    }
    function add_option($num, $comp) {
        /*Build option tag.
        */
        $this->content .= "<option";
        if ($comp == $num)
            $this->content .= ' selected="selected"';
        $this->content .= ">{$num}</option>\n";
    }
    function get() {
        /*Get generated content.
        */
        return $this->content;
    }
}

class ProgressBar {
    //Progress bar content
    var $content;
    //Base link
    var $link;
    function ProgressBar($config) {
        /*Progress Bar is
        << ... 2 3 4 ... >>
        */
        $this->content = '<div id="progress">';
        $this->link = http_build_query(array('search'=>$config->query_string_orig,
                    'options'=>$config->options, 'dirs'=>$config->dirs,
                    'step'=>$config->query_step));
    }
    function previous_page($val) {
        //display <<
        $this->page($val, '&lt;&lt;');
    }
    function next_page($val) {
        //display >>
        $this->page($val, '&gt;&gt;');
    }
    function dot() {
        //display ...
        $this->content .= '<span>...</span>';
    }
    function page($val, $value) {
        //display 2 3 4
        $this->content .= "<a href='?{$this->link}&count={$val}' class='pg'>$value</a>";
    }
    function current_page($value) {
        //display current page
        $this->content .= "<span class='pg' id='pg'>{$value}</span>";
    }
    function get() {
        //Get generated progress bar
        $this->content .= "</div>\n";
        return $this->content;
    }
}

class Content {
    var $content;
    var $config;
    var $found;

    function Content($total, $size, $config) {
        $this->config = $config;
        $this->total = $total;
        $this->content = "<div id='content'>";
        if ($this->total > 1) {
            $result = _('results found');
            $display = _('shown:');
        } else {
            $result = _('result found');
            $display = _('shown:');
        }
        $this->found .= "<div id='found'>" . _('For') . ' "<b>'.
                $this->config->query_string_orig . '</b>", ' .
                $this->total . ' ' . $result . ", {$size} " .
                $display . "</div>\n";
    }

    function convert_url($url) {
        if ($this->config->config['encode_url'] === '1')
            $url = urlencode($url);
        return $this->config->config['base_url'] . $url;
    }

    function timestamp_to_date($stamp) {
        return date('d-m-y H:i', $stamp);
    }

    function convert_size($size) {
        //convert size to xxxGo, xxxMo or xxxko
        if ($size >= 1024*1024*1024)
            return round(($size / 1024)/1024/1024, 2) ." Go";
        elseif ($size >= 1024*1024)
            return round(($size / 1024)/1024, 2) ." Mo";
        elseif ($size >= 1024)
            return round(($size / 1024), 2) ." ko";
        else
            return $size;
    }

    function highlight($needle, $haystack){
        $carac = array(' ', '"', ',', '.', ';', '?', '@', '-', '_', '/', "'", '(', ')');
        $ind = stripos($haystack, $needle);
        if($ind === false)
            return $haystack;
        $len = strlen($needle);
        //hightlight par not ppar, part, ...
        if (!in_array($haystack[$ind - 1], $carac) or !in_array($haystack[$ind+$len], $carac))
            return substr($haystack, 0, $ind + $len) . $this->highlight($needle, substr($haystack, $ind + $len));
        return substr($haystack, 0, $ind) . "<b>" . substr($haystack, $ind, $len) . "</b>" .
                $this->highlight($needle, substr($haystack, $ind + $len));
    }

    function display_debug($text) {
        $this->content .= "\n<!-- debug:\n". $text . "\n-->\n";
    }

    function display_info($lines, $percent) {
        $line_array = array();
        $lines = explode(PHP_EOL, $lines);
        for($i = 0; $i < count($lines); ++$i) {
            $line = preg_split('/=/i', $lines[$i], 2);
            $line_array[$line[0]] = $line[1];
        };
        $filename = $line_array['url'];
        if ($percent > 66)
            $percent_class = 'percent_high';
        elseif ($percent > 33)
            $percent_class = 'percent_middle';
        else
            $percent_class = 'percent_low';
        $sample = $line_array['sample'];
        $words = explode(' ', $this->config->query_string_orig);
        $type_mime = str_replace('/', '-', $line_array['type']);
        $icon = "themes/" . $this->config->theme . "/img/icons/" . $type_mime . ".png";
        if (! is_file($icon))
            $icon = "themes/" . $this->config->theme . "/img/icons/unknown.png";
        $this->content .= "<div class='document'><div class='icon'><img src='" . $icon . "' alt='" . $type_mime . "'/></div><div class='link'><a href='";
        $this->content .= $this->convert_url($filename);
        for ($i = 0; $i < count($words); ++$i) {
            $filename = $this->highlight($words[$i], $filename);
            $sample = $this->highlight($words[$i], $sample);
        }
        $this->content .= "'>{$filename}</a></div>\n";
        $this->content .= "<div class='info'><ul><li>" . _('relevant:');
        $this->content .= "<table class='percent'><tr>\n";
        //small space on left if percent is 100%
        if ($percent != 100) {
            $this->content .= "<td>\n</td>";
        }
        $this->content .= "<td class='$percent_class' width='$percent%'></td></tr></table>";
        $this->content .= "</li>" .
                "<li class='middle'>" . _("size: ") . $this->convert_size($line_array['size']) .
                "</li><li>" . _("modified: ") .
                $this->timestamp_to_date($line_array['modtime']) . "</li></ul></div>\n";
        $this->content .= "<div class='sample'>" . $sample . '</div>';
        $this->content .= "</div>\n";
    }

    function progress_bar() {
        // << ... 2 3 4 .. >>
        //if number of resultat small than query stop, no progress_bar
        if ($this->total <= $this->config->query_step)
            return '</div>'; //end 'content'
        $content = new ProgressBar($this->config);
        //first page to display, minimum 1
        $page = max(1, floor($this->config->query_count / $this->config->query_step) -
                    floor(($this->config->config['max_progressbar_values'] - 1) / 2) + 1) ;
        //add '<<' if needed
        if ($this->config->query_count > 0)
            $content->previous_page($this->config->query_count - $this->config->query_step);
        //add '...' if first page is not 1
        if ($page > 1)
            $content->dot();
        //last_page is last page for all search
        $last_page = ceil($this->total / $this->config->query_step);
        //max_page is last display page
        $max_page = min($last_page,
                        $page + $this->config->config['max_progressbar_values'] - 1);
        //parse all display page
        while ($page <= $max_page) {
            $document = ($page - 1) * $this->config->query_step;
            if ($document == $this->config->query_count)
                $content->current_page($page);
            else
                $content->page($document, $page);
            $page += 1;
        }
        if ($page - 1 < $last_page)
            $content->dot();
        //add '>>' if needed
        if ($this->config->query_count + $this->config->query_step < $this->total)
            $content->next_page($this->config->query_count + $this->config->query_step);
        $this->content .= '</div>'; //end 'content'
        return $content->get();
    }

    function get() {
        return $this->content;
    }
}
?>
