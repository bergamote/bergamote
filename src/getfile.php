<?php
/* Begamote, the file search engine for EOLE
 * Copyright (C) 2013 Emmanuel Garette
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include "lib.php";

$file=$_GET["file"];

$config = new Config(true);
$ftp_user = eolephpCAS::getUser();
$code_err = array();
$code_msg = array();
$ftp_pass = eolephpCAS::retrievePT($config->config['ftp_service'], $code_err,
    $code_msg);

header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
#header('Content-Length: ' . filesize($file));
header('Content-Disposition: attachment; filename=' . basename($file));

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'ftp://'.$ftp_user.':'.$ftp_pass.'@'.
				$config->config['ftp_host'].$file);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_exec($ch);
curl_close($ch);
?>
