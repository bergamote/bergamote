<?php
/* Begamote, the file search engine for EOLE
 * Copyright (C) 2013 Emmanuel Garette
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include "lib.php";
include "content.php";
$config = new Config();
if($config->tracker_url != '')
    $content_header = '<script src="' . $config->tracker_url . '"></script>';
else
    $content_header = '';
$field = new FieldSet($config);
$content_field = $field->get();
if ( $config->query_string != '') {
    $cont = display_search($config);
    $content_found = $cont->found;
    $content_body = $cont->content;
    $content_progress_bar = $cont->progress_bar();
} else {
    $content_body = '';
    $content_progress_bar = '';
}
include 'themes/' . $config->theme . '/tmpl/index.tmpl';
?>
