<?php
/* Begamote, the file search engine for EOLE
 * Copyright (C) 2013 Emmanuel Garette
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define('__CAS_URL', '');

class Config {
    //content configuration file
    var $config;
    //options in GET
    var $options;
    //true if some options specified by user
    var $has_options;
    //dirs in GET
    var $dirs;
    //number of result in a page, set from GET or default_step
    var $query_step;
    //sort type, set from GET or default_sort
    var $query_sort;
    //all available options
    var $available_options;
    //all available dirs, own group and 'perso'
    var $available_dirs;
    //list of xapian dabatase directory used for query
    var $databases;
    //number of the first result
    var $query_count;
    //query
    var $query_string;
    var $query_string_orig;
    //add tracker
    var $tracker_url;
    //CAS informations
    var $infos_cas;
    //theme
    var $theme;
    //query flags
    var $flags_support;
    var $flags;
    //opensearch
    var $is_opensearch;
    //debug
    var $debug;

    function Config($proxy=false) {
        /*Load config object:
        - config is an array() load from /etc/bergamote/config.ini file.
        - options, dirs, query_step, query_string and query are load from $_GET
        - available_dirs and available_options are calculated.
        */
        //
        $this->config = parse_ini_file("/etc/bergamote/config.ini", false);
        //getext
        $locale = $this->config['locale'];
        $domain = 'bergamote';

        putenv("LANG=" . $locale);
        putenv("LANGUAGE=" . $locale);
        setlocale(LC_MESSAGES, $locale);
        bindtextdomain($domain, dirname(__FILE__).'/locale');
        bind_textdomain_codeset($domain, 'UTF-8');

        textdomain($domain);
        //
        if ($this->config['authenticated_user'] === '1')
            cas_auth($proxy);
        $this->options = $_GET['options'];
        if (isset($_GET['opensearch']) and $_GET['opensearch'] != '')
            $this->is_opensearch = true;
        else
            $this->is_opensearch = false;
        if ($this->options === null and ! $this->is_opensearch) {
            $this->has_options = false;
            $this->options = array();
        } else {
            $this->has_options = true;
        }
        //
        $this->query_step = intval($_GET['step']);
        if ($this->query_step < 1 or $this->query_step > 100)
            $this->query_step = intval($this->config['default_step']);
        $this->query_sort = $_GET['sort'];
        if (! in_array($this->query_sort, array(_('relevant'), _('date'), _('size'))))
            $this->query_sort = $this->config['default_sort'];
        //
        if ($this->config['index_filename'] === '1')
            $this->available_options = array('file', 'filename');
        else {
            $this->available_options = array('file');
            $this->options = $this->available_options;
        }
        if ($this->is_opensearch)
            $this->options = $this->available_options;
        //
        $this->gen_dirs();
        //
        if (isset($_GET['count']) and is_numeric($_GET['count']))
            $this->query_count = intval($_GET['count']);
        else
            $this->query_count = 0;
        //
        if (! $this->has_options)
            $this->reset_conf();
        else
            $this->gen_search();
        $this->tracker_url = $this->config['tracker_url'];
        if ($this->tracker_url != '' &&
                $this->config['authenticated_user'] === '1') {
            $this->infos_cas = eolephpCAS::getDetails();
            if (isset($this->infos_cas['infos']['ENTPersonProfils'][0])) {
                $profil = $this->infos_cas['infos']['ENTPersonProfils'][0];
                $this->tracker_url .= $profil;
            }
        }
        $this->theme = $this->config['theme'];
        $this->flags_support = $this->config['flags'];
        if ($this->flags_support === '1')
            $this->set_flags();
        $this->debug = $this->config['debug'];
    }

    function reset_conf() {
        /*If options is empty, reset options, dirs  and query_string to
        default values.
        Reset conf is needed if no options specified.
        */
        $this->options = $this->available_options;
        if ($this->config['force_default_check'])
            $this->dirs = array($this->config['force_default_check']);
        else
            $this->dirs = $this->available_dirs;
        $this->query_string = '';
        $this->query_string_orig = '';
    }
    function gen_default_dirs() {
    }
    function gen_dirs() {
        /*Generate variables related to directory:
        - dirs: list of activate directories
        - available_dirs: list of available dirs for this user
        - databases: local directory name.

        Available_dirs are list of shares send by SSO server plus 'perso'.
        Directories listed in 'forbidden_dirs' are remove to this list.
        Special directory 'perso' cannot be remove.

        Databases is build with all 'dirs' name for each 'options'.

        This function valid dirs variable and options too.
        */
        //
        $this->dirs = $_GET['dirs'];
        if ($this->dirs === null)
            $this->dirs = array();
        $this->databases = array();
        $databases = array();
        $this->available_dirs = array();
        //
        $forbidden_dirs = explode(' ', $this->config['forbidden_dirs']);
        if ($this->config['authenticated_user'] === '1') {
            if ($this->config['indexing_group'] === '1') {
                $this->infos_cas = eolephpCAS::getDetails();
                $this->available_dirs = $this->infos_cas['utilisateur']['shares'];
                for ($i = 0; $i < count($this->available_dirs); ++$i) {
                    $current_dir = $this->available_dirs[$i];
                    if (in_array($current_dir, $forbidden_dirs))
                        unset($this->available_dirs[$i]);
                    else
                        $databases[$current_dir] =
                                    $this->config['omega_dir_group'] .
                                    "/{$current_dir}";
                }
            }
            //reindex
            $this->available_dirs = array_values($this->available_dirs);
            if ($this->config['indexing_user'] === '1') {
                $username = eolephpCAS::getUser();
                $this->available_dirs[] = 'perso';
                $databases['perso'] = $this->config['omega_dir_user'] .
                                        "/{$username}";
            }
        } else {
            //if no authentification an indexing_group is true, liste directory
            //no indexing_user support available
            if ($this->config['indexing_group'] === '1') {
                $dirs = scandir($this->config['omega_dir_group']);
                for ($i = 0; $i < count($dirs); ++$i) {
                    $current_dir = $dirs[$i];
                    $current_dir_path = $this->config['omega_dir_group'] . "/{$current_dir}";
                    if (! in_array($current_dir, array('.', '..')) and is_dir($current_dir_path) and ! in_array($current_dir, $forbidden_dirs)){
                        $databases[$current_dir] = $current_dir_path;
                        $this->available_dirs[] = $current_dir;
                    }
                }
            }
        }
        if ($this->config['force_indexing_extra_dir'] != '' and
                $this->config['force_indexing_extra_omega_dir'] != '' and
                $this->config['force_indexing_extra_name'] != '') {
            $name = $this->config['force_indexing_extra_name'];
            $this->available_dirs[] = $name;
            $databases[$name] =
                $this->config['force_indexing_extra_omega_dir'] . "/" .
                $this->config['force_indexing_extra_name'];

        }
        //if only one dir and not index_filename, no options available so force has_options
        if (count($this->available_dirs) === 1 and $this->config['index_filename'] === '0') {
            $this->has_options = true;
        }
        //
        if ($this->is_opensearch)
            $this->dirs = $this->available_dirs;

        if ($this->has_options) {
            if (count($this->available_dirs) != 1)
                for ($i = 0; $i < count($this->dirs); ++$i) {
                    $dir = $this->dirs[$i];
                    if (! in_array($dir, $this->available_dirs))
                        error(_("forbidden directory"));
                    $this->add_database($databases, $dir);
                }
            else {
                $key = array_keys($databases);
                $this->add_database($databases, $key[0]);
            }
        }
    }

    function add_database($databases, $dir) {
        $l = $databases[$dir];
        if (is_dir($l + '/filename'))
            error(_('Database not updated, please run bergamote-index before'));
        if (is_dir($l))
            $this->databases[] = $l;
    }
    function gen_search() {
        /*If $_GET['search'], valid it and return value.
        Only some characters are allowed.
        */
        if ((isset($_GET['search']) and $_GET['search'] != '') or
            $this->is_opensearch)
        {
            $is_file = false;
            $is_filename = false;
            if (in_array('file', $this->options) and
                    in_array('file', $this->available_options))
                $is_file = true;
            if (in_array('filename', $this->options) and
                    in_array('filename', $this->available_options))
                $is_filename = true;
            if ($this->is_opensearch)
            {
                $query_string = $_GET['opensearch'];
            } else {
                $query_string = $_GET['search'];
            }
            //allow ':' only allowed for ext
            if (preg_match('/([^t]):|([^x])t:|([^e])xt:|^t:|^xt:|^:/', $query_string))
                error(_('only ext is allowed as prefix'));
            //remove forbidden characters
            $query_string = preg_replace('/[^\p{L}0-9a-z_\-\. \'&:]/u', '', strtolower($query_string));
            $this->query_string_orig = $query_string;
            $this->query_string = '( ';
            if ($is_file){
                $this->query_string .= $query_string;
            }
            if ($is_file and $is_filename){
                $this->query_string .= ' OR ';
            }
            if ($is_filename)
                $this->query_string .= 'filename:' . $query_string;
            if ($this->config['authenticated_user'] === '1') {
                //username
                $username = eolephpCAS::getUser();
                $this->query_string .= ' ) AND ( write:@' . $username;
                $except = '';
                //groupname
                $this->query_string .= ' OR ( ( ';
                $this->infos_cas = eolephpCAS::getDetails();
                $cnt = count($this->infos_cas['utilisateur']['user_groups']);
                for($i = 0; $i < $cnt; ++$i) {
                    $grp = $this->infos_cas['utilisateur']['user_groups'][$i];
                    if ($i != 0)
                        $this->query_string .= ' OR ';
                    $this->query_string .= ' write:#' . $grp;
                    $except .= ' NOT group:' . $grp;
                };
                $this->query_string .= ' ) NOT user:' . $username . ' )';
                //other
                $this->query_string .= ' OR ( write:* NOT user:' . $username .
                    $except;
                $this->query_string .= ' )';
            };
            $this->query_string .= ' )';
        } else {
            $this->query_string = '';
            $this->query_string_orig = '';
        }
    }

    function set_flags() {
        $this->flags = XapianQueryParser::FLAG_PHRASE | XapianQueryParser::FLAG_BOOLEAN;
        if ($this->config['allow_love_hate'])
            $this->flags |= XapianQueryParser::FLAG_LOVEHATE;
        if ($this->config['allow_synonym'])
            if ($this->config['force_synonym'])
                $this->flags |= XapianQueryParser::FLAG_AUTO_SYNONYMS;
            else
                $this->flags |= XapianQueryParser::FLAG_SYNONYM;
    }
}

function display_search($config) {
    /*Connect to Xapian database and query values
    We can connect to different database.

    :param config: Config object
    :return: result content
    */
    include "xapian.php";

    try {
        //Open the database
        if ($config->databases === array())
            error(_("no database select"));
        $database = new XapianDatabase($config->databases[0]);

        for($i = 1; $i < count($config->databases); ++$i) {
            $newbase = new XapianDatabase($config->databases[$i]);
            $database->add_database($newbase);
        };
        //Start an enquire session.
        $enquire = new XapianEnquire($database);

        //Query and stem
        $qp = new XapianQueryParser();
        $stemmer = new XapianStem($config->config[lang]);
        $qp->set_stemmer($stemmer);
        $qp->set_database($database);
        $qp->set_stemming_strategy(XapianQueryParser::STEM_SOME);
        $qp->add_boolean_prefix("user", "O");
        $qp->add_boolean_prefix("group", "G");
        $qp->add_boolean_prefix("write", "I");
        $qp->add_boolean_prefix("filename", "F");
        $qp->add_boolean_prefix("ext", "E");
        if ($config->flags_support === '1')
            $query = $qp->parse_query($config->query_string, $config->flags);
        else
            $query = $qp->parse_query($config->query_string);

        //Start $config->query_step documents from $config->query_count
        $enquire->set_query($query);
        if($config->query_sort === 'date')
            $enquire->set_sort_by_value_then_relevance(0);
        else if($config->query_sort === 'taille')
            $enquire->set_sort_by_value_then_relevance(2);
        $matches = $enquire->get_mset($config->query_count, $config->query_step, 0);
        $size = $matches->size();

        //Display the results.
        $total = $matches->get_matches_estimated();
        $content = new Content($total, $size, $config);
        if ($config->debug)
            $content->display_debug("Parsed query is: {$query->get_description()} - (" . $config->query_string . ")");
        $begin = $matches->begin();
        while (!$begin->equals($matches->end())) {
            $content->display_info($begin->get_document()->get_data(),
                        $begin->get_percent());
            $begin->next();
        }

    } catch (Exception $e) {
        error($e->getMessage());
    }
    return $content;
}

function cas_auth($proxy=false){
    require_once('CAS-1.3.1/eoleCAS.php');
    require_once('configCAS/cas.inc.php');

    session_name ("bergamote");

    if ($proxy == true)
        eolephpCAS::proxy(__CAS_VERSION, __CAS_SERVER, __CAS_PORT, __CAS_URL, true);
    else
        eolephpCAS::client(__CAS_VERSION, __CAS_SERVER, __CAS_PORT, __CAS_URL, true);

    if (__CAS_VALIDER_CA) {
        eolephpCAS::setCasServerCACert(__CAS_CA_LOCATION);
    } else {
        if (method_exists(eolephpCAS, 'setNoCasServerValidation')){
            eolephpCAS::setNoCasServerValidation();
        }
    }

    if (__CAS_LOGOUT){
        if (method_exists('eolephpCAS', 'eolelogoutRequests')){
            eolephpCAS::EoleLogoutRequests(false);
        }
    }

    eolephpCAS::forceAuthentication();
    if (!eolephpCAS::isAuthenticated()){
        error(_("no authentification"));
    }
}
?>
